<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(["prefix" => "yonetim", "namespace" => "Yonetim"], function () {
    Route::get('/', function () {
        return redirect()->route("yonetim.oturumac");
    });

    Route::match(['get', 'post'], "/oturumac", "YoneticiController@oturumac")->name("yonetim.oturumac");
    Route::get('/oturumkapat', 'YoneticiController@oturumKapat')->name('yonetim.oturumkapat');

    Route::group(['middleware' => 'yonetim'], function () {

        Route::get('/anasayfa', 'AnasayfaController@anasayfa')->name('yonetim.anasayfa');

        Route::group(['prefix' => 'kullanici'], function () {
            Route::match(['get', 'post'], '/', 'KullaniciController@index')->name('yonetim.kullanici');
            Route::get('/create', 'KullaniciController@form')->name('yonetim.kullanici.create');
            Route::get('/duzenle/{id?}', 'KullaniciController@form')->name('yonetim.kullanici.duzenle');
            Route::match(['get', 'post'],'/kaydet/{id?}', 'KullaniciController@kaydet')->name('yonetim.kullanici.kaydet');
            Route::get('/sil/{id}', 'KullaniciController@sil')->name('yonetim.kullanici.sil');
        });

        Route::group(['prefix' => 'kategori'], function () {
            Route::match(['get', 'post'], '/', 'KategoriController@index')->name('yonetim.kategori');
            Route::get('/create', 'KategoriController@create')->name('yonetim.kategori.create');
            Route::get('/duzenle/{id?}', 'KategoriController@duzenle')->name('yonetim.kategori.duzenle');
            Route::match(['get', 'post'],'/kaydet/{id?}', 'KategoriController@kaydet')->name('yonetim.kategori.kaydet');
            Route::get('/sil/{id}', 'KategoriController@sil')->name('yonetim.kategori.sil');
        });

        Route::group(['prefix' => 'urun'], function () {
            Route::match(['get', 'post'], '/', 'UrunController@index')->name('yonetim.urun');
            Route::get('/create', 'UrunController@create')->name('yonetim.urun.create');
            Route::get('/duzenle/{id?}', 'UrunController@duzenle')->name('yonetim.urun.duzenle');
            Route::match(['get', 'post'],'/kaydet/{id?}', 'UrunController@kaydet')->name('yonetim.urun.kaydet');
            Route::get('/sil/{id}', 'UrunController@sil')->name('yonetim.urun.sil');
        });
    });

});

Route::get("/", "AnasayfaController@index")->name("anasayfa");

Route::get("/kategori/{slug_kategoriadi}", "KategoriController@index")->name("kategori");

Route::get("/urun/{slug_urunadi}", "UrunController@index")->name("urun");

Route::post("/ara", "UrunController@ara")->name("urun_ara");
Route::get("/ara", "UrunController@ara")->name("urun_ara");
Route::group(["prefix" => "sepet"], function () {
    Route::get("/", "SepetController@index")->name("sepet");
    Route::patch("/guncelle/{id}", "SepetController@adet");
    Route::post("/ekle", "SepetController@ekle")->name("sepet.ekle");
    Route::delete("/kaldir/{rowId}", "SepetController@kaldir")->name("sepet.kaldir");
    Route::delete("/bosalt", "SepetController@bosalt")->name("sepet.bosalt");
});
Route::get("/odeme", "OdemeController@index")->name("odeme");
Route::post("/odeme", "OdemeController@odemeyap")->name("odemeyap");
Route::group(["middleware" => "auth"], function () {

    Route::get("/siparisler", "SiparisController@index")->name("siparisler");
    Route::get("/siparis/{id}", "SiparisController@detay")->name("siparis");
});


Route::group(["prefix" => "kullanici"], function () {
    Route::post("/cikis", "KullaniciController@cikis")->name("kullanici.cikis");
    Route::get("/aktiflestir/{aktivasyon_anahtari}", "KullaniciController@aktiflestir")->name("kullanici.aktiflestir");
    Route::get("/oturumac", "KullaniciController@oturumac")->name("kullanici.oturumac");
    Route::post("/oturumac", "KullaniciController@giris");
    Route::get("/kaydol", "kullaniciController@kaydol_form")->name("kullanici.kaydol");
    Route::post("/kaydol", "kullaniciController@kaydol");
    Route::get("/sifremiunuttum", function () {
        return redirect()->route("anasayfa");
    })->name("kullanici.sifre_form");
});
Route::get("/deneme", function () {
    return new App\Mail\KullaniciKayitMail(\App\Models\Kullanici::find(1));
    /* DB::enableQueryLog();
     $gunun_firsati = App\Models\UrunDetay::with("urun")->where("goster_gunun_firsati","")->get();

     dd(DB::getQueryLog());*/
});

