<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiprarisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siparis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("sepet_id")->unsigned()->unique();
            $table->decimal("siparis_tutari",6,2);
            $table->string("durum",30);
            $table->string("banka",20);
            $table->integer("taksit_sayisi")->nullable();
            $table->string("adsoyad",50)->nullable();
            $table->string("adres",200)->nullable();
            $table->string("telefon",50)->nullable();
            $table->string("ceptelefonu",50)->nullable();

            $table->timestamp("olusturma_tarihi")->default(DB::raw("CURRENT_TIMESTAMP"));
            $table->timestamp("guncelleme_tarihi")->default(DB::raw("CURRENT_TIMESTAMP on UPDATE CURRENT_TIMESTAMP"));
            $table->timestamp("silinme_tarihi")->nullable();

            $table->foreign("sepet_id")->references("id")->on("sepet")->onDelete("cascade");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siparis');
    }
}
