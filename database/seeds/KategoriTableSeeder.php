<?php

use Illuminate\Database\Seeder;

class KategoriTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("kategori")->truncate();


        $ust_id = DB::table("kategori")->insertGetId([
            "kategori_adi"=>"Teknoloji",
            "slug"=>"teknoloji",
            ]);
            DB::table("kategori")->insertGetId(["kategori_adi"=>"Bilgisayar","slug"=>"bilgisayar","ust_id"=>$ust_id]);
            DB::table("kategori")->insertGetId(["kategori_adi"=>"Telefon","slug"=>"telefon","ust_id"=>$ust_id]);
            DB::table("kategori")->insertGetId(["kategori_adi"=>"Oyun","slug"=>"oyun","ust_id"=>$ust_id]);
            DB::table("kategori")->insertGetId(["kategori_adi"=>"Drone","slug"=>"drone","ust_id"=>$ust_id]);
            DB::table("kategori")->insertGetId(["kategori_adi"=>"Tablet","slug"=>"tablet","ust_id"=>$ust_id]);
        $ust_id = DB::table("kategori")->insertGetId([
            "kategori_adi"=>"Beyaz Eşya",
            "slug"=>"beyaz-esya"
        ]);
             DB::table("kategori")->insertGetId(["kategori_adi"=>"Çamaşır Makinesi","slug"=>"camasir_makinesi","ust_id"=>$ust_id]);
             DB::table("kategori")->insertGetId(["kategori_adi"=>"Buz Dolabı","slug"=>"buzdolabi","ust_id"=>$ust_id]);
             DB::table("kategori")->insertGetId(["kategori_adi"=>"Fırın","slug"=>"fırın","ust_id"=>$ust_id]);

        $ust_id = DB::table("kategori")->insertGetId([
            "kategori_adi"=>"Giyim",
            "slug"=>"giyim"
        ]);
             $ust_id2 =  DB::table("kategori")->insertGetId(["kategori_adi"=>"Kadın","slug"=>"kadin","ust_id"=>$ust_id]);
                DB::table("kategori")->insertGetId(["kategori_adi"=>"Tshort","slug"=>"tshort","ust_id"=>$ust_id2]);
                DB::table("kategori")->insertGetId(["kategori_adi"=>"Pantolon","slug"=>"pantolon","ust_id"=>$ust_id2]);
                DB::table("kategori")->insertGetId(["kategori_adi"=>"Gömlek","slug"=>"gomlek","ust_id"=>$ust_id2]);
                DB::table("kategori")->insertGetId(["kategori_adi"=>"Gecelik","slug"=>"gecelik","ust_id"=>$ust_id2]);

               $ust_id2 = DB::table("kategori")->insertGetId(["kategori_adi"=>"Erkek","slug"=>"erkek","ust_id"=>$ust_id]);
                DB::table("kategori")->insertGetId(["kategori_adi"=>"Tshort","slug"=>"tshort","ust_id"=>$ust_id2]);
                DB::table("kategori")->insertGetId(["kategori_adi"=>"Pantolon","slug"=>"pantolon","ust_id"=>$ust_id2]);
                DB::table("kategori")->insertGetId(["kategori_adi"=>"Gömlek","slug"=>"gomlek","ust_id"=>$ust_id2]);
                DB::table("kategori")->insertGetId(["kategori_adi"=>"Gecelik","slug"=>"gecelik","ust_id"=>$ust_id2]);

        DB::table("kategori")->insertGetId(["kategori_adi"=>"Çocuk","slug"=>"cocuk","ust_id"=>$ust_id]);
             DB::table("kategori")->insertGetId(["kategori_adi"=>"Bebek","slug"=>"bebek","ust_id"=>$ust_id]);

        $ust_id = DB::table("kategori")->insertGetId([
            "kategori_adi"=>"Mobilya",
            "slug"=>"mobilya"
        ]);
        $ust_id = DB::table("kategori")->insertGetId([
            "kategori_adi"=>"Kamp",
            "slug"=>"kamp"
        ]);
        DB::table("kategori")->insertGetId([
            "kategori_adi"=>"Kategori 1",
            "slug"=>"kategori-1"
        ]);
        DB::table("kategori")->insertGetId([
            "kategori_adi"=>"Kategori 2",
            "slug"=>"kategori-2"
        ]);
        DB::table("kategori")->insertGetId([
            "kategori_adi"=>"Kategori 3",
            "slug"=>"kategori-3"
        ]);
        DB::table("kategori")->insertGetId([
            "kategori_adi"=>"Kategori 4",
            "slug"=>"kategori-4"
        ]);

    }
}
