<?php

use App\Models\Kullanici;
use App\Models\KullaniciDetay;
use Illuminate\Database\Seeder;

class KullaniciSedeer extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker\Generator $faker)
    {
        Kullanici::truncate();
        KullaniciDetay::truncate();
        $yoneteci = Kullanici::create([
            'email' => 'gokhangunes.4@gmail.com',
            'sifre' => Hash::make('123456'),
            'adsoyad' => 'Gökhan GÜNEŞ',
            'kullanici_adi' => 'gokhangunes',
            'aktif_mi' => 1,
            'user_role' => 2,
        ]);

        $yoneteci->detay()->create([
            'adres' => 'Atatürk Mah, İstanbul',
            'ceptelefonu' => '(534) 361 2051',
            'telefon' => '(534) 361 2051',
        ]);

        for ($i = 0; $i < 50; $i++) {
            $kullanici = Kullanici::create([
                'email' => $faker->unique()->safeEmail,
                'sifre' => Hash::make('123456'),
                'adsoyad' => $faker->name,
                'kullanici_adi' => $faker->unique()->userName,
                'aktif_mi' => 1,
                'user_role' => 1,
            ]);

            $kullanici->detay()->create([
                'adres' => $faker->address,
                'ceptelefonu' => $faker->e164PhoneNumber,
                'telefon' =>  $faker->e164PhoneNumber,
            ]);
        }
    }
}
