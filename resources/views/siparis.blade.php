@extends("layoults.master")
@section("title","Siparis")
@section("content")

    <div class="container">
        <div class="bg-content">
            <a href="{{route('siparisler')}}" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-arrow-left"></i> Siparişlere Dön </a>
            <h2>Sipariş (SP-00123)</h2>
            <table class="table table-bordererd table-hover">
                <tr>
                    <th colspan="2">Ürün</th>
                    <th>Tutar</th>
                    <th>Adet</th>
                    <th>Ara Toplam</th>
                    <th>Durum</th>
                </tr>
                @foreach($siparis->sepet->sepeturun as $sepetUrun)
                <tr>
                    <td> <img src="http://lorempixel.com/120/100/food/2"></td>
                    <td>{{$sepetUrun->urun->urun_adi}}</td>
                    <td>{{round($sepetUrun->fiyati, 2)}}</td>
                    <td>{{$sepetUrun->adet}}</td>
                    <td>{{round($sepetUrun->araToplam(), 2)}}</td>
                    <td>
                        {{$sepetUrun->durum}}
                    </td>
                </tr>
                @endforeach
                <tr>
                    <th></th>
                    <th></th>
                    <th>Toplam Tutar (KDV Dahil)</th>
                    <td>{{$siparis->siparisTutari()}}</td>
                    <th></th>
                </tr>
                <tr>
                    <th></th>
                    <th></th>
                    <th>Kargo</th>
                    <td>Ücretsiz</td>
                    <th></th>
                </tr>
                <tr>
                    <th></th>
                    <th></th>
                    <th>Sipariş Toplamı</th>
                    <td>{{$siparis->siparisTutari()}}</td>
                    <th></th>
                </tr>

            </table>
        </div>
    </div>

@endsection
