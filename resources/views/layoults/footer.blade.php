<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-6">Eticaret eğitim &copy; 2020</div>
            <div class="col-md-6 text-right"><a href="">Gökhan Güneş</a> <a href="{{url()->previous()}}" class=""><i class="fa fa-arrow-circle-o-left"></i> </a>
            </div>
        </div>
    </div>
</footer>
