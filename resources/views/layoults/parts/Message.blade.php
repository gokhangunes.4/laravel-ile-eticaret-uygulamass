@if(session()->has("mesaj_tur"))
    <div class="container alert-ustbar">
    @if(session("pozisyon")=="ustbar")
        <div class="alert  alert-{{session("mesaj_tur")}}">
            {{session("mesaj")}}
        </div>
    @endif
    </div>
@endif

<div class="container alert-box" style="display: none">
        <div class="alert alert-success">
            <span class="alert-mesaj">succes</span>
        </div>

</div>
