<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>@yield("title") | Gökhan Ticaret</title>
        @include("layoults.head")
</head>

<body id="commerce">
@include("layoults.navbar")
@include("layoults.parts.Message")
@yield("content")
@include("layoults.footer")
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript"  src="/js/app.js"></script>
@yield("footer")
</body>

</html>
