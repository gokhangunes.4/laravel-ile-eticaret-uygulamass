@extends("layoults.master")
@section("title","Baslık")
@section("content")

    <div class="container">

        <div class="row">
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-heading">Kategoriler</div>
                    <div class="list-group categories">

                        @foreach($anaKategoriler as $kategori)
                        <a href="{{route("kategori",$kategori->slug)}}" class="list-group-item"><i class="fa fa-arrow-circle-o-right"></i> {{$kategori->kategori_adi}}</a>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <ol class="carousel-indicators">
                        @foreach($slider_goster as $i =>$urun)
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="{{$i==0?"active":""}}"></li>
                        @endforeach
                    </ol>
                    <div class="carousel-inner" role="listbox">
                        @foreach($slider_goster as $i =>  $urun)
                        <div class="item {{$i == 0 ?"active":""}}">
                            <img src="http://lorempixel.com/640/400/food/1" alt=" {{$urun->aciklama}}">
                            <div class="carousel-caption">
                                {{$urun->urun_adi}}
                            </div>
                        </div>
                            @endforeach
                    </div>
                    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <div class="col-md-3">
                <div class="panel panel-default" id="sidebar-product">
                    <div class="panel-heading">Günün Fırsatı</div>
                    <div class="panel-body">

                        <a href="#">
                            <img src="http://lorempixel.com/400/485/food/1" class="img-responsive">
                            <p style="text-align:center;font: bold">{{$gunun_firsati->urun->urun_adi}}</p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="products">
            <div class="panel panel-theme">
                <div class="panel-heading">Öne Çıkan Ürünler</div>
                <div class="panel-body">
                    <div class="row">
                        @foreach($one_cikan as $urun)
                        <div class="col-md-3 product">
                            <a href="{{route("urun",$urun->slug)}}"><img src="https://via.placeholder.com/400" alt="{{$urun->aciklama}}">
                            <p>{{$urun->urun_adi}}</p>
                            </a>
                            <p class="price">{{$urun->fiyati}} ₺</p>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="products">
            <div class="panel panel-theme">
                <div class="panel-heading">Çok Satan Ürünler</div>
                <div class="panel-body">
                    <div class="row">
                        @foreach($cok_satan as $urun)
                        <div class="col-md-3 product">
                            <a href="{{route("urun",$urun->slug)}}"><img src="https://via.placeholder.com/400">
                                <p>{{$urun->urun_adi}}</p></a>
                            <p class="price">{{$urun->fiyati}} ₺</p>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="products">
            <div class="panel panel-theme">
                <div class="panel-heading">İndirimli Ürünler</div>
                <div class="panel-body">
                    <div class="row">
                        @foreach($indirimli as $urun)
                            <div class="col-md-3 product">
                                <a href="{{route("urun",$urun->slug)}}"><img src="https://via.placeholder.com/400">
                                    <p>{{$urun->urun_adi}}</p></a>
                                <p class="price">{{$urun->fiyati}} ₺</p>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
