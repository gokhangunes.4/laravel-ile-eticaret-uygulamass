@extends("layoults.master")
@section("title",$kategori->kategori_adi )
@section("content")
    <div class="container">
        <ol class="breadcrumb">

            <li><a href="{{route("anasayfa")}}">Anasayfa</a></li>

            <li class="active"><a href="#">{{$kategori->kategori_adi}}</a></li>

        </ol>
        <div class="row">
            <div class="col-md-3">
                <div class="panel panel-default">
                    <div class="panel-heading">{{$kategori->kategori_adi}}</div>
                    <div class="panel-body">
                        @if(count($alt_kategori)>0)
                        <h3>Alt Kategoriler</h3>
                        <div class="list-group categories">
                            @foreach($alt_kategori as $kategori)
                            <a href="{{route("kategori",$kategori->slug)}}" class="list-group-item"><i class="fa fa-arrow-circle-o-right"></i> {{$kategori->kategori_adi}}</a>
                            @endforeach
                          </div>
                        @else
                            <h5>Alt Kategori Bulunamadı</h5>
                        @endif
                        <h3>Fiyat Aralığı</h3>
                        <form>
                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"> 100-200
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox"> 200-300
                                    </label>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="products bg-content">
                    @if(count($urunler)<=0)
                        <div class="col-md-5 product">  <p> Bu Kategoriye Ait Ürün Bulunamadı</p></div>
                    @else
                    Sırala
                    <a href="?order=coksatanlar" class="btn btn-default">Çok Satanlar</a>
                    <a href="?order=yeni" class="btn btn-default">Yeni Ürünler</a>
                    <hr>
                    <div class="row">

                        @foreach($urunler as $urun)
                            <div class="col-md-3 product">
                                <a href="{{route("urun",$urun->slug)}}"><img src="https://via.placeholder.com/400"></a>
                                <p><a href="{{route("urun",$urun->slug)}}">{{$urun->urun_adi}}</a></p>
                                <p class="price">{{$urun->fiyati}} ₺</p>
                                <p><a href="#" class="btn btn-theme">Sepete Ekle</a></p>
                             </div>
                        @endforeach

                    </div>
                        {{ request()->has("order") ? $urunler->appends(["order"=>request("order")])->links(): $urunler->links()}}
                @endif
                </div>

            </div>
        </div>
    </div>

@endsection
