@extends("layoults.master")
@section("title","Siparisler")
@section("content")
    <div class="container">
        <div class="bg-content">
            <h2>Siparişler</h2>
            <p>Henüz siparişiniz yok</p>
            <table class="table table-bordererd table-hover">
                <tr>
                    <th>Sipariş Kodu</th>
                    <th>Sipariş Tarihi</th>
                    <th>KDV</th>
                    <th>Adet</th>
                    <th>Toplam Tutar</th>
                    <th>Durum</th>
                    <th>İşlem</th>
                </tr>
                @foreach($siparisler as $siparis)
                <tr>
                    <td>SP-00{{$siparis->id}}</td>
                    <td>{{$siparis->olusturma_tarihi}}</td>
                    <td>{{round($siparis->kdvTutari(), 2)}}</td>
                    <td>{{$siparis->sepet->sepetUrunAdet()}}</td>
                    <td>{{ round($siparis->siparisTutari(), 2)}}</td>
                    <td>
                        {{$siparis->durum}}
                    </td>
                    <td><a href="{{route('siparis', $siparis->id)}}" class="btn btn-sm btn-success">Detay</a></td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>

@endsection
