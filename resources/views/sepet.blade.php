@extends("layoults.master")
@section("title","Sepet")
@section("content")

    <div class="container">

        <div class="bg-content">
            <h2>Sepet</h2>

            @if(count(Cart::content())>0)
            <table class="table table-bordererd table-hover">
                <tr>
                    <th colspan="2">Ürün</th>
                    <th>Adet Fiyatı</th>
                    <th>Adet</th>

                    <th class="text-right">Tutar</th>
                </tr>


                @foreach(Cart::content() as $urunItem)
                <tr>
                    <td>
                        <a href="{{route("urun",$urunItem->options->slug)}}">
                            <img src="https://via.placeholder.com/60">
                        </a>

                    </td>
                    <td>
                        <a href="{{route("urun",$urunItem->options->slug)}}"> {{$urunItem->name}}</a>
                        <form action="{{route("sepet.kaldir",$urunItem->rowId)}}" method="post">
                            {{csrf_field()}}
                            {{method_field("DELETE")}}
                            <input type="submit" class="btn  btn-danger btn-xs" value="Sepetten Kaldır">
                        </form>
                    </td>

                    <td>{{$urunItem->price}} ₺</td>
                    <td>
                        <a href="#" class="btn btn-xs btn-default urun-adet-azalt" data-id="{{$urunItem->rowId}}" data-item="{{$urunItem->qty-1}}">-</a>
                        <span class="data-adet" style="padding: 10px 20px">{{$urunItem->qty}} </span>
                        <a href="#" class="btn btn-xs btn-default urun-adet-arttır" data-id="{{$urunItem->rowId}}" data-item="{{$urunItem->qty+1}}">+</a>
                    </td>
                    <td class="text-right">{{$urunItem->subtotal}} ₺</td>

                </tr>
                @endforeach
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th class="text-right">Toplam tutar</th>
                    <th class="text-right">{{Cart::subtotal()}} ₺</th>

                </tr>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th class="text-right">Toplam KDV</th>
                    <th class="text-right">{{Cart::tax()}} ₺</th>

                </tr>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th class="text-right">Toplam Tutar (KDV Dahil)</th>
                    <th class="text-right">{{Cart::total()}} ₺</th>

                </tr>




            </table>
                <div>

                    <a href="{{route("odeme")}}" class="btn btn-success pull-right btn-lg">Ödeme Yap</a>
                    <form action="{{route("sepet.bosalt")}}" method="post">
                        {{csrf_field()}}
                        {{method_field("DELETE")}}
                        <input type="submit" class="btn btn-info pull-left" value="Sepeti Boşalt">
                    </form>
                </div>
            @else
                <h3> Sepetinizde ürün yok</h3>
            @endif

        </div>
    </div>


    </div>


@endsection

@section("footer")
    <script>
        $(function (){

            $(".urun-adet-azalt,.urun-adet-arttır").on("click", function () {
                var yeni_adet = Number($(this).attr("data-item"));
                var id = $(this).attr("data-id");
                var mesaj;
                $(this).parent().find(".urun-adet-azalt").attr("data-item",yeni_adet-1);
                $(this).parent().find(".urun-adet-arttır").attr("data-item",(yeni_adet)+1);
                $(this).parent().find(".data-adet").text(yeni_adet);

                if(yeni_adet==0)
                {

                        $(this).parent().parent().hide();


                }
                $.ajax({
                    type:"PATCH",
                    url:"/sepet/guncelle/"+id,
                    data:{
                        adet:yeni_adet,
                        id:id
                    },
                    success:function (mesaj){
                        $(".alert-box").show();
                        setTimeout(function () {
                            $(".alert-box").slideDown(500);
                        }, 4000);
                        setTimeout(function () {
                            $(".alert-box").slideUp(500);
                        }, 4000);
                        $(".alert-mesaj").text(mesaj);
                    }
                })

            });

        });
    </script>
@endsection
