@extends("layoults.master");
@section("title","Urun Arama")
@section("content")
    <div class="container">
        <div class="">
            <div class="products bg-content">
                    <div class="row">
                        @foreach($urunler as $urun)
                            <div class="col-md-3 product">
                                <a href=""><img src="https://via.placeholder.com/400"></a>
                                <p><a href="">{{$urun->urun_adi}}</a></p>
                                <p class="price">{{$urun->fiyati}} ₺</p>
                                <p><a href="#" class="btn btn-theme">Sepete Ekle</a></p>
                            </div>
                        @endforeach
                    </div>
                {{$urunler->appends(["aranan"=>old("aranan")])->links()}}
            </div>
        </div>

    </div>
@endsection
