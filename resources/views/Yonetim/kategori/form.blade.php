@extends('yonetim.layoults.master')
@section('title','Kategori')
@section('content')

    <h1 class="page-header"> Kategori Yönetim</h1>
    <form action="{{route('yonetim.kategori.kaydet', @$kategori->id)}}">
        {{csrf_field()}}
        <div class="pull-right">
            <button type="submit" class="btn btn-primary">
                {{@$kategori->id > 0 ?"Güncelle":"Kayıt"}}
            </button>

        </div>
        <h2 class="sub-header">
            Kategori {{@$kategori->id > 0 ?"Düzenle":"Ekle"}}
        </h2>
        @include('layoults.parts.errors')
        @include('layoults.parts.Message')

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="formUstIdSelect">Üst Kategori</label>
                    <select name="ust_id" id="formUstIdSelect" class="form-control">
                        <option value="" >Ana Kategori</option>
                        @foreach($kategoriler as $item)
                            <option value="{{$item->id}}" {{$kategori->ust_id == $item->id?'selected':''}}>{{$item->kategori_adi}}</option>
                        @endforeach

                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="formKategoriAdiInput">Kategori Adı</label>
                    <input type="text" value="{{old('kategori_adi', $kategori->kategori_adi)}}" class="form-control"
                           name="kategori_adi"
                           id="formKategoriAdiInput" placeholder="Kategori Adı">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="formslugInput">Kategori Slug</label>
                    <input type="hidden" value="{{old('orginal_slug', $kategori->slug)}}" class="form-control" name="orginal_slug">
                    <input type="text" value="{{old('slug', $kategori->slug)}}" class="form-control" name="slug"
                           id="formslugInput" placeholder="slug">
                </div>
            </div>
        </div>


    </form>
@endsection
@section('footer')
    <script>
        $(function () {
            var tr = ["ş", "Ş", "ı", "(", ")", "‘", "ü", "Ü", "ö", "Ö", "ç", "Ç", " ", "/", "*", "?", "ş", "Ş", "ı", "ğ", "Ğ", "İ", "ö", "Ö", "Ç", "ç", "ü", "Ü"];
            var en = ["s", "S", "i", "", "", "", "u", "U", "o", "O", "c", "C", "-", "-", "-", "", "s", "S", "i", "g", "G", "I", "o", "O", "C", "c", "u", "U"];

            $("#formKategoriAdiInput").keyup(function (event) {
                var kategori_adi = $('#formKategoriAdiInput').val();
                kategori_adi = $.trim(kategori_adi);
                kategori_adi = kategori_adi.replace(/[^a-z0-9-]/gi, '-')
                    .replace(/-+/g, '-')
                    .replace(/^-|-$/g, '')
                    .toLowerCase();

                $('#formslugInput').val(kategori_adi);

            });
        });
    </script>
@endsection
