@extends('Yonetim.layoults.master')
@section('title', 'Kullanici List')
@section('content')
    <h1 class="page-header"> Kullanici Yönetim</h1>

    <h3 class="sub-header"> Kullanıcı Listesi</h3>
    <div class="well">
        <div class="btn-group pull-right">
            <a href="{{route('yonetim.kullanici.create')}}" type="button" class="btn btn-primary float-left">Yeni
                Kayıt</a>
        </div>
        <form method="post" action="{{route('yonetim.kullanici')}}" class="form-inline">
            {{csrf_field()}}
            <div class="form-group">
                <label for="aranan"> Ara</label>
                <input type="text" name="aranan" class="form-control form-control-sm" id="aranan"
                       placeholder="Ad, Email, Tel, Adres Ara..." value="{{ old('aranan') }}">
            </div>
            <button class="btn btn-primary"> Ara</button>
        </form>

    </div>

    <div class="table-responsive">
        <table class="table table-hover table-bordered">
            <thead class="thead-dark">
            <tr>
                <th>#</th>
                <th>Adı Soyadı</th>
                <th>Email</th>
                <th>Aktif Mi</th>
                <th>Kayıt Tarihi</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($list as $entry)
                <tr>
                    <td>{{$entry->id}}</td>
                    <td>{{$entry->adsoyad}}</td>
                    <td>{{$entry->email}}</td>
                    <td>
                        @if($entry->aktif_mi)
                            <span class="label label-success">Aktif</span>
                        @else
                            <span class="label label-warning">Pasif</span>
                        @endif
                    </td>
                    <td>{{$entry->olusturma_tarihi}}</td>
                    <td style="width: 100px">
                        <a href="{{route('yonetim.kullanici.duzenle', $entry->id)}}" class="btn btn-xs btn-success"
                           data-toggle="tooltip" data-placement="top" title=""
                           data-original-title="Tooltip on top">
                            <span class="fa fa-pencil"></span>
                        </a>
                        <a href="{{route('yonetim.kullanici.sil', $entry->id)}}" class="btn btn-xs btn-danger"
                           data-toggle="tooltip" data-placement="top" title=""
                           onclick="return confirm('Emin Misiniz?')" data-original-title="Tooltip on top">
                            <span class="fa fa-trash"></span>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $list->links() }}
    </div>
@endsection
