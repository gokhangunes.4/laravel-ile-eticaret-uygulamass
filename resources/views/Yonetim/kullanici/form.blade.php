@extends('yonetim.layoults.master')
@section('title','Kullanıcı')
@section('content')

    <h1 class="page-header"> Kullanıcı Yönetim</h1>
    <form action="{{route('yonetim.kullanici.kaydet', @$kullanici->id)}}">
        {{csrf_field()}}
        <div class="pull-right">
            <button type="submit" class="btn btn-primary">
                {{@$kullanici->id > 0 ?"Güncelle":"Kayıt"}}
            </button>

        </div>
        <h2 class="sub-header">
            Kullanıcı {{@$kullanici->id > 0 ?"Düzenle":"Ekle"}}
        </h2>
        @include('layoults.parts.errors')
        @include('layoults.parts.Message')
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="formTelefonInput">Kullanıcı Adı</label>
                    <input type="text" value="{{old('kullanici_adi', $kullanici->kullanici_adi)}}" class="form-control"
                           name="kullanici_adi" id="formKullaniciAdiInput" placeholder="Kullanici Adı">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="formEmailInput">Email address</label>
                    <input type="email" value="{{old('email', $kullanici->email)}}" class="form-control" name="email"
                           id="formEmailInput" placeholder="Email">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="formPasswordInput">Şifre</label>
                    <input type="sifre" class="form-control" name="sifre" id="formPasswordInput" placeholder="Şifre">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="formAdSoyadInput">Ad Soyad</label>
                    <input type="text" value="{{old('adsoyad', $kullanici->adsoyad)}}" class="form-control"
                           name="adsoyad"
                           id="formAdSoyadInput" placeholder="Ad Soyad">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="formTelefonInput">Telefon</label>
                    <input type="text" value="{{old('telefon', $kullanici->detay->telefon)}}" class="form-control" name="telefon"
                           id="formTelefonInput" placeholder="Telefon No">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="formCepTelefonInput">Cep Telefonu</label>
                    <input type="text" class="form-control" value="{{old('ceptelefonu', $kullanici->detay->ceptelefonu)}}"
                           name="ceptelefonu" id="formCepTelefonInput" placeholder="Cep Telefon No">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="formAdresInput">Adres</label>
                    <input type="text" value="{{old('adres', $kullanici->detay->adres)}}" class="form-control" name="adres"
                           id="formAdresInput" placeholder="Adres">
                </div>
            </div>
        </div>

        <div class="checkbox">
            <label>
                <input type="checkbox"  name="aktif_mi" {{old('aktif_mi', $kullanici->aktif_mi) ?'checked':''}}> Aktif Mi
            </label>
        </div>

    </form>
@endsection
