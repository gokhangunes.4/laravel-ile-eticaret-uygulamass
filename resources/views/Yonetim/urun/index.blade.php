@extends('Yonetim.layoults.master')
@section('title', 'Kategori')
@section('content')
    <h1 class="page-header"> Kategori Yönetim</h1>

    <h3 class="sub-header"> Kategori Listesi</h3>

    @include('layoults.parts.errors')
    @include('layoults.parts.Message')
    <div class="well">
        <div class="btn-group pull-right">
            <a href="{{route('yonetim.urun.create')}}" type="button" class="btn btn-primary float-left">Yeni
                Kayıt</a>
        </div>
        <form method="post" action="{{route('yonetim.urun')}}" class="form-inline">
            {{csrf_field()}}
            <div class="form-group">
                <label for="aranan"> Ara</label>
                <input type="text" name="aranan" class="form-control form-control-sm" id="aranan"
                       placeholder="Ad" value="{{ old('aranan') }}">
            </div>
            <button class="btn btn-primary"> Ara</button>
        </form>

    </div>

    <div class="table-responsive">
        <table class="table table-hover table-bordered">
            <thead class="thead-dark">
            <tr>
                <th>#</th>
                <th>Ürün Adı</th>
                <th>Kategori Slug</th>
                <th>Kayıt Tarihi</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($urunler as $urun)
                <tr>
                    <td>{{$urun->id}}</td>
                    <td>{{$urun->urun_adi}}</td>
                    <td>{{$urun->slug}}</td>
                    <td>{{$urun->olusturma_tarihi}}</td>
                    <td style="width: 100px">
                        <a href="{{route('yonetim.urun.duzenle', $urun->id)}}" class="btn btn-xs btn-success"
                           data-toggle="tooltip" data-placement="top" title=""
                           data-original-title="Tooltip on top">
                            <span class="fa fa-pencil"></span>
                        </a>
                        <a href="{{route('yonetim.urun.sil', $urun->id)}}" class="btn btn-xs btn-danger"
                           data-toggle="tooltip" data-placement="top" title=""
                           onclick="return confirm('Emin Misiniz?')" data-original-title="Tooltip on top">
                            <span class="fa fa-trash"></span>
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        {{ $urunler->links() }}
    </div>
@endsection
