@extends('yonetim.layoults.master')
@section('title','Kategori')
@section('content')

    <h1 class="page-header"> Üyün Yönetim</h1>
    <form action="{{route('yonetim.urun.kaydet', @$urun->id)}}">
        {{csrf_field()}}
        <div class="pull-right">
            <button type="submit" class="btn btn-primary">
                {{@$urun->id > 0 ?"Güncelle":"Kayıt"}}
            </button>

        </div>
        <h3 class="sub-header">
            Ürün {{@$urun->id > 0 ?"Düzenle":"Ekle"}}
        </h3>
        @include('layoults.parts.errors')
        @include('layoults.parts.Message')
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="formUrunAdiInput">Ürün Adı</label>
                    <input type="text" value="{{old('urun_adi', $urun->urun_adi)}}" class="form-control"
                           name="urun_adi"
                           id="formUrunAdiInput" placeholder="Ürün Adı">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="formslugInput">Kategori Slug</label>
                    <input type="hidden" value="{{old('orginal_slug', $urun->slug)}}" class="form-control"
                           name="orginal_slug">
                    <input type="text" value="{{old('slug', $urun->slug)}}" class="form-control" name="slug"
                           id="formslugInput" placeholder="slug">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="formAcikalmaTextarea">Açıklama</label>

                    <textarea class="form-control" name="aciklama" id="formslugInput"
                              placeholder="Açiklama">{{old('aciklama', $urun->aciklama)}}</textarea>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="formFiyatiInput">Ürün Fiyat</label>

                    <input type="text" value="{{old('fiyati', $urun->fiyati)}}" class="form-control" name="fiyati"
                           id="formFiyatiInput" placeholder="Fiyati">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-2">
                <input type="hidden" value="0" name="goster_slider">
                <input type="checkbox" id="checkboxGosterSlider" name="goster_slider" value="1"
                       class="form-group" {{old('goster_slider', $urun->detay->goster_slider ?'checked':'')}}>
                <label for="checkboxGosterSlider"> Göster Slider</label>
            </div>
            <div class="col-md-2">
                <input type="hidden" value="0" name="goster_gunun_firsati">
                <input type="checkbox" id="checkboxGosterGununFirsati" name="goster_gunun_firsati" value="1"
                       class="form-group" {{old('goster_gunun_firsati', $urun->detay->goster_gunun_firsati ?'checked':'')}}>
                <label for="checkboxGosterGununFirsati"> Göster Günün Fırsatı</label>
            </div>
            <div class="col-md-2">
                <input type="hidden" value="0" name="goster_one_cikan">
                <input type="checkbox" id="checkboxOneCikan" name="goster_one_cikan" value="1"
                       class="form-group" {{old('goster_one_cikan', $urun->detay->goster_one_cikan ?'checked':'')}}>
                <label for="checkboxOneCikan"> Göster Öne Çıkan</label>
            </div>
            <div class="col-md-2">
                <input type="hidden" value="0" name="goster_cok_satan">
                <input type="checkbox" id="checkboxCokSatan" name="goster_cok_satan" value="1"
                       class="form-group" {{old('goster_cok_satan', $urun->detay->goster_cok_satan ?'checked':'')}}>
                <label for="checkboxCokSatan"> Göster Çok Satan</label>
            </div>
            <div class="col-md-2">
                <input type="hidden" value="0" name="goster_indirimli">
                <input type="checkbox" id="checkboxIndirimli" name="goster_indirimli" value="1"
                       class="form-group" {{old('goster_indirimli', $urun->detay->goster_indirimli ?'checked':'')}}>
                <label for="checkboxIndirimli"> Göster İndirimli</label>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="formUstIdSelect">Üst Kategori</label>
                        <select name="ust_id" id="formUstIdSelect" class="form-control" multiple>
                            @foreach($urun->kategoriler as $kategori)
                            <option value="" selected>{{$kategori->kategori_adi}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>

    </form>
@endsection
@section('footer')
    <script>
        $(function () {
            var tr = ["ş", "Ş", "ı", "(", ")", "‘", "ü", "Ü", "ö", "Ö", "ç", "Ç", " ", "/", "*", "?", "ş", "Ş", "ı", "ğ", "Ğ", "İ", "ö", "Ö", "Ç", "ç", "ü", "Ü"];
            var en = ["s", "S", "i", "", "", "", "u", "U", "o", "O", "c", "C", "-", "-", "-", "", "s", "S", "i", "g", "G", "I", "o", "O", "C", "c", "u", "U"];

            $("#formUrunAdiInput").keyup(function (event) {
                var kategori_adi = $('#formUrunAdiInput').val();
                kategori_adi = $.trim(kategori_adi);
                kategori_adi = kategori_adi.replace(/[^a-z0-9-]/gi, '-')
                    .replace(/-+/g, '-')
                    .replace(/^-|-$/g, '')
                    .toLowerCase();

                $('#formslugInput').val(kategori_adi);

            });
        });
    </script>
@endsection
