<?php

namespace App\Http\Middleware;

use Closure;

use Auth;
class YonetimMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::guard('yonetim')->check() && Auth::guard('yonetim')->user()->user_role)
        {
            return $next($request);
        }

        return redirect()->route('yonetim.oturumac');
    }
}
