<?php

namespace App\Http\Controllers;

use App\Models\Siparis;
use Illuminate\Http\Request;
use mysql_xdevapi\Exception;

class SiparisController extends Controller
{
    public function index(){
        $siparisler = Siparis::with('sepet')
            ->whereHas('sepet', function ($query) {
                $query->where('kullanici_id', auth()->id());
            })->get();

        return view("siparisler", compact('siparisler'));
    }
    public function detay($id){
        try {

        $siparis = Siparis::with('sepet')
            ->whereHas('sepet', function ($query) {
                return $query->where('kullanici_id', auth()->id());
            })
            ->findOrFail($id);

        return view("siparis", compact('siparis'));

        } catch (Exception $exception) {
            return $exception->getMessage();
        }
    }
}
