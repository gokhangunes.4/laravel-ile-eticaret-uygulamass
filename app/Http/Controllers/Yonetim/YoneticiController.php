<?php

namespace App\Http\Controllers\Yonetim;

use Auth;

use App\Http\Controllers\Controller;

class YoneticiController extends Controller
{
    public function oturumac()
    {
        if (request()->isMethod('post')) {


            $this->validate(request(), [
                'email' => 'required|email',
                'sifre' => 'required',
            ]);

            if (Auth::guard('yonetim')->attempt([
                'email' => request()->get('email'),
                'password' => request()->get('sifre'),
                'user_role' => [2, 3],
            ], request()->has('benihatirla'))) {
                return redirect()->route('yonetim.anasayfa');
            } else {
                return back()->withInput()->withErrors(['email' => 'Giriş Bilgileriniz Hatalı']);
            }
        }
        return view("yonetim.oturumac");
    }

    public function oturumKapat()
    {
        Auth::guard('yonetim')->logout();
        session()->flush();

        return redirect()->route('yonetim.oturumac');
    }
}
