<?php

namespace App\Http\Controllers\Yonetim;

use App\Models\Kategori;
use Illuminate\Http\Request;
use mysql_xdevapi\Exception;
use App\Http\Controllers\Controller;

class KategoriController extends Controller
{
    public function index()
    {
        request()->flash();

        if (request()->filled('aranan')) {
            $aranan = request()->get('aranan');

            $kategoriler = Kategori::where('kategori_adi', 'like', "%$aranan%")
                ->orWhere('slug', 'like', "%$aranan%")
                ->paginate(8)
                ->appends(['aranan' => $aranan]);
        } else {
            $kategoriler = Kategori::paginate(8);
        }

        return view('yonetim.kategori.index', compact('kategoriler'));
    }

    public function create()
    {
        $kategori = new Kategori();

        $kategoriler = Kategori::all();

        return view('yonetim.kategori.form', compact('kategori', 'kategoriler'));
    }

    public function duzenle($id)
    {
        $kategori = Kategori::find($id);

        $kategoriler = Kategori::all();

        return view('yonetim.kategori.form', compact('kategori', 'kategoriler'));
    }

    public function kaydet($id = 0)
    {

        $this->validate(request(), [
            'kategori_adi' => 'required|min:2',
            'slug' => 'required|min:2|' . (request()->get('orginal_slug') == request()->get('slug') ? '' : 'unique:kategori,slug'),
        ]);

        $data = request()->only('ust_id', 'kategori_adi', 'slug');

        if ($id == 0) {
            $kategori = Kategori::create($data);
        } else {
            $kategori = Kategori::find($id);
            $kategori->update($data);
        }

        return redirect()->route('yonetim.kategori.duzenle', $kategori->id)
            ->with('mesaj_tur', 'success')
            ->with('mesaj', 'İşlem Tamamlandı')
            ->with('pozisyon', 'ustbar');
    }

    public function sil($id)
    {
        Kategori::destroy($id);

        return redirect()->route('yonetim.kategori')
            ->with('mesaj_tur', 'success')
            ->with('mesaj', 'İşlem Tamamlandı')
            ->with('pozisyon', 'ustbar');
    }
}
