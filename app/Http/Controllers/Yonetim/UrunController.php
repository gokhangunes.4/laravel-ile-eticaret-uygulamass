<?php

namespace App\Http\Controllers\Yonetim;

use App\Models\Urun;
use App\Models\Kategori;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;

class UrunController extends Controller
{
    public function index()
    {
        request()->flash();

        if (request()->filled('aranan')) {
            $aranan = request()->get('aranan');

            $urunler = Urun::where('urun_adi', 'like', "%$aranan%")
                ->orWhere('slug', 'like', "%$aranan%")
                ->paginate(8)
                ->appends(['aranan' => $aranan]);
        } else {
            $urunler = Urun::paginate(8);
        }

        return view('yonetim.urun.index', compact('urunler'));
    }

    public function create()
    {
        $urun = new Urun();

        $kategoriler = Kategori::all();

        return view('yonetim.urun.form', compact('urun', 'kategoriler'));
    }

    public function duzenle($id)
    {
        $urun = Urun::find($id);

        $kategoriler = Kategori::all();

        return view('yonetim.urun.form', compact('urun', 'kategoriler'));
    }

    public function kaydet($id = 0)
    {

        $this->validate(request(), [
            'urun_adi' => 'required|min:2',
            'slug' => 'required|min:2|' . (request()->get('orginal_slug') == request()->get('slug') ? '' : 'unique:urun,slug'),
        ]);

        $data = request()->only( 'urun_adi', 'slug', 'fiyati', 'aciklama', 'fiyati');
        $dataDetay = request()->only( 'goster_slider', 'goster_gunun_firsati', 'goster_one_cikan', 'goster_indirimli', 'goster_cok_satan');

        if ($id == 0) {
            $urun = Urun::create($data);
            $urun->detay()->create($dataDetay);
        } else {
            $urun = Urun::find($id);
            $urun->update($data);
            $urun->detay()->update($dataDetay);
        }

        return redirect()->route('yonetim.urun.duzenle', $urun->id)
            ->with('mesaj_tur', 'success')
            ->with('mesaj', 'İşlem Tamamlandı')
            ->with('pozisyon', 'ustbar');
    }

    public function sil($id)
    {
        $urun = Urun::find($id);
        $urun->kategoriler()->detach();
        $urun->detay()->delete();
        $urun->delete();

        return redirect()->route('yonetim.urun')
            ->with('mesaj_tur', 'success')
            ->with('mesaj', 'İşlem Tamamlandı')
            ->with('pozisyon', 'ustbar');
    }
}
