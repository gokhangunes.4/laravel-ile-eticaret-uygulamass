<?php

namespace App\Http\Controllers\Yonetim;

use Auth;
use Hash;
use App\Models\Kullanici;
use Illuminate\Http\Request;
use mysql_xdevapi\Exception;
use App\Models\KullaniciDetay;
use App\Http\Controllers\Controller;

class KullaniciController extends Controller
{
    public function index()
    {
        if (request()->filled('aranan')) {
            request()->flash();

            $aranan = request()->get('aranan');

            $list = Kullanici::with('detay')
                ->where('kullanici.adsoyad', 'like', "%$aranan%")
                ->orWhere('kullanici.email', 'like', "%$aranan%")
                ->orWhere('kullanici.kullanici_adi', 'like', "%$aranan%")
                ->orWhereHas('detay', function ($query) use ($aranan) {
                    $query->where('ceptelefonu', 'like', "%$aranan%");
                    $query->orWhere('telefon', 'like', "%$aranan%");
                    $query->orWhere('adres', 'like', "%$aranan%");
                })
                ->paginate(8)
                ->appends(['aranan' => $aranan]);
        } else {
            $list = Kullanici::orderByDesc('olusturma_tarihi')->paginate(8);
        }

        return view('yonetim.kullanici.index', compact('list'));
    }

    public function form($id = 0)
    {
        try {
            $kullanici = new Kullanici();
            $kullanici->detay = new KullaniciDetay();
            if ($id != 0) {
                $kullanici = Kullanici::find($id);
            }

            return view('yonetim.kullanici.form', compact('kullanici'));
        } catch (Exception $exception) {
            dd($exception);
        }

    }

    public function kaydet($id = 0)
    {
        $this->validate(request(), [
            'email' => 'required|email',
            'adsoyad' => 'required|min:2',
        ]);
        $data = request()->only('adsoyad', 'email', 'kullanici_adi');
        $data['aktif_mi'] = request()->has('aktif_mi');
        $data['detay'] = \request()->only('adres', 'telefon', 'ceptelefonu');

        if (request()->filled('sifre')) {
            $data['sifre'] = Hash::make(request()->get('sifre'));
        }

        if ($id > 0) {
            $kullanici = Kullanici::find($id);
            $kullanici->update($data);

        } else {
            $this->validate(request(), [
                'email' => 'required|email|unique:kullanici',
                'adsoyad' => 'required|min:2',
            ]);
            $kullanici = Kullanici::create($data);
        }
        $kullanici->detay()->updateOrCreate($data['detay']);

        return redirect()->route('yonetim.kullanici.duzenle', $kullanici->id)
            ->with('mesaj_tur', 'success')
            ->with('mesaj', 'Başarıyla ' . $id > 0 ? 'Düzenleme işlemi başarıyla tamamlandı' : 'Kayıt işlemi tamamlandı')
            ->with("pozisyon", "ustbar");
    }

    public function sil($id)
    {
        Kullanici::destroy($id);

        return redirect()->route('yonetim.kullanici')
            ->with('mesaj_tur', 'success')
            ->with('mesaj', 'Kullanıcı başarıyla silindi.')
            ->with("pozisyon", "ustbar");
    }
}
