<?php

namespace App\Http\Controllers;

use App\Models\Urun;
use Illuminate\Http\Request;

class UrunController extends Controller
{
    public function index($urun_slug){
        $urun = Urun::where("slug","=",$urun_slug)->firstOrFail();
        $kategoriler = $urun->kategoriler()->distinct()->get();
        return view("urun",compact("urun","kategoriler"));
    }
    public function ara(){
        $aranan = request()->input("aranan");
        $urunler = Urun::where("urun_adi","LIKE","%$aranan%")
            ->orWhere("aciklama","like","%$aranan%")
            ->paginate(8);
        request()->flash();
        return view("arama",compact("urunler"));
    }
}
