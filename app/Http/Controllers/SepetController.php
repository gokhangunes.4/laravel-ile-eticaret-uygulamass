<?php

namespace App\Http\Controllers;

use App\Models\SepetUrun;
use App\Models\Urun;
use App\Models\Sepet;
use Cart;
use Illuminate\Http\Request;

class SepetController extends Controller
{
    public function index()
    {

        return view("sepet");
    }

    public function ekle()
    {
        $urun = Urun::find(request("id"));

        $cartItems = Cart::add($urun->id, $urun->urun_adi, 1, $urun->fiyati, ["slug" => $urun->slug]);

        if (auth()->check()) {
            $aktif_sepet_id = session("aktif_sepet_id");

            if (!isset($aktif_sepet_id)) {
                $aktif_sepet_id = Sepet::aktifSepetId();

                session()->put("aktif_sepet_id", $aktif_sepet_id);
            }
            $sepet_urun = SepetUrun::updateOrCreate(
                ["sepet_id" => $aktif_sepet_id, "urun_id" => $urun->id],
                [
                    "urun_id" => $urun->id,
                    "adet" => $cartItems->qty,
                    "fiyati" => $urun->fiyati,
                    "durum" => "beklemede",
                ]
            );
        }

        return redirect("sepet")
            ->with("mesaj", "Ürün Sepete Eklendi")
            ->with("mesaj_tur", "success")
            ->with("pozisyon", "ustbar");
    }

    public function kaldir($rowId)
    {
        if (auth()->check()) {
            $aktif_sepet_id = session("aktif_sepet_id");
            $cartItem = Cart::get($rowId);
            SepetUrun::where("sepet_id", $aktif_sepet_id)->where("urun_id", $cartItem->id)->delete();
        }
        Cart::remove($rowId);
        return redirect("sepet")->with("mesaj", "Ürün Sepetten Kaldırıldı")
            ->with("mesaj_tur", "success")
            ->with("pozisyon", "ustbar");
    }

    public function bosalt()
    {
        if (auth()->check()) {
            $aktif_sepet_id = session("aktif_sepet_id");
            SepetUrun::where("sepet_id", $aktif_sepet_id)->delete();
        }
        Cart::destroy();
        return redirect("sepet")->with("mesaj", "Sepetiniz Boşaltıldı.")
            ->with("mesaj_tur", "success")
            ->with("pozisyon", "ustbar");
    }

    public function adet($rowId)
    {
        if (auth()->check()) {

            $aktif_sepet_id = session("aktif_sepet_id");
            $cartItem = Cart::get($rowId);
            if (request("adet") == 0) {
                SepetUrun::where("sepet_id", $aktif_sepet_id)->where("urun_id", $cartItem->id)->delete();
            } else {

                SepetUrun::where("sepet_id", $aktif_sepet_id)
                    ->where("urun_id", $cartItem->id)
                    ->update(["adet" => request("adet")]);
            }
        }
        Cart::update($rowId, request("adet"));
        return "Başarıyla Güncellendi";
    }
}
