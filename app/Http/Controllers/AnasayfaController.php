<?php

namespace App\Http\Controllers;
use Illuminate\Database;
use App\Models\Kategori;
use App\Models\UrunDetay;
use App\Models\Urun;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AnasayfaController extends Controller
{
    public function index(){

        $anaKategoriler = Kategori::whereNull("ust_id")->take(8)->get();
        $gunun_firsati = UrunDetay::with("urun")
            ->where("goster_gunun_firsati","","1")
            ->first();

        $slider_goster = Urun::join("urun_detay","urun_detay.urun_id","urun.id")
            ->where("urun_detay.goster_slider",1)
            ->orderBy("guncelleme_tarihi","desc")
            ->take(4)
            ->get();

        $one_cikan = Urun::join("urun_detay","urun_detay.urun_id","urun.id")
            ->where("goster_one_cikan",1)
            ->orderBy("guncelleme_tarihi","desc")
            ->take(4)
            ->get();

        $cok_satan = Urun::join("urun_detay","urun_detay.urun_id","urun.id")
            ->where("goster_cok_satan",1)
            ->orderBy("guncelleme_tarihi","desc")
            ->take(4)
            ->get();

        $indirimli = Urun::join("urun_detay","urun_detay.id","urun.id")
            ->where("goster_indirimli",1)
            ->orderBy("guncelleme_tarihi","desc")
            ->take(4)
            ->get();

        return view("anasayfa",compact("anaKategoriler","gunun_firsati","slider_goster","one_cikan","cok_satan","indirimli"));
    }
}
