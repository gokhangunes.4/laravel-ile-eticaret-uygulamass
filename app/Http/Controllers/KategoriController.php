<?php

namespace App\Http\Controllers;

use App\Models\Kategori;
use Illuminate\Http\Request;

class KategoriController extends Controller
{
    public function index($slug_kategoriadi){
        $kategori = Kategori::whereSlug($slug_kategoriadi)->firstOrFail();
        $alt_kategori = Kategori::where("ust_id",$kategori->id)->get();

        $order = request("order");

        if($order=="coksatanlar"){
            $urunler = $kategori->urunler()
                ->join("urun_detay","urun_detay.urun_id","urun.id")
                ->orderByDesc("urun_detay.goster_cok_satan")
                ->distinct()
                ->paginate(8);

        }
        elseif ($order=="yeni"){
            $urunler = $kategori->urunler()
                ->join("urun_detay","urun_detay.urun_id","urun.id")
                ->orderByDesc("guncelleme_tarihi")
                ->distinct()
                ->paginate(8);
        }
        else{
            $urunler = $kategori->urunler()->distinct()->paginate(8);

        }

        return  view("kategori",compact("kategori","alt_kategori","urunler"));
    }

}
