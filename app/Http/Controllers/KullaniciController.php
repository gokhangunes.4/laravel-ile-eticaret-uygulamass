<?php

namespace App\Http\Controllers;

use App\Mail\KullaniciKayitMail;
use App\Models\Kullanici;
use App\Models\KullaniciDetay;
use App\Models\SepetUrun;
use App\Models\Sepet;
use App\Models\Urun;
use Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class KullaniciController extends Controller
{
    public function __construct()
    {
        $this->middleware("guest")->except("cikis");
    }

    public function oturumac()
    {
        return view("kullanici.oturumac");
    }

    public function kaydol_form()
    {
        return view("kullanici.kaydol");
    }

    public function giris()
    {
        $this->validate(request(), [
            'email' => 'required|email',
            'sifre' => 'required',

        ]);
        if (auth()->attempt(["email" => request('email'), 'password' => request("sifre"), 'aktif_mi' => 1], request()->has("benihatirla"))) {
            request()->session()->regenerate();
            if (!is_null(auth()->user()->aktivasyon_anahtari)) {
                $this->oturumsonlandir();
                return redirect()->route("kullanici.oturumac")
                    ->with("pozisyon", "ustbar")
                    ->with("mesaj", "Hesabınız aktif değildir. Mail adresinize gelen aktivasyon linkine tıklayınız.")
                    ->with("mesaj_tur", "info");
            }

            auth()->user()->sepetDoldur();

            return redirect()->intended("/")
                ->with("pozisyon", "ustbar")
                ->with("mesaj", "Başarıyla Giriş Yapıldı")
                ->with("mesaj_tur", "success");
        } else {
            $errors = ['email' => "Giriş bilgileri hatalı"];
            return back()->withErrors($errors);
        }
    }

    public function oturumsonlandir()
    {
        auth()->logout();
        request()->session()->flush();
        request()->session()->regenerate();

    }

    public function cikis()
    {
        $this->oturumsonlandir();
        return redirect()->to("")
            ->with("pozisyon", "ustbar")
            ->with("mesaj", "Başarıyla Çıkış Yapıldı")
            ->with("mesaj_tur", "success");
    }

    public function kaydol()
    {
        $this->validate(request(), [
            'adsoyad' => 'required|min:5|max:60', // Zorunlu 5 karakter
            'email' => 'required|email|unique:kullanici',  // Zorunlu email olmalı ve veritabanında aynı mail adresi olmamalı
            'sifre' => 'required|confirmed|min:5|max:15',
            'kullanici_adi' => 'required|unique:kullanici|min:4|max:12',
        ]);
        if (request("adsoyad") != null) {
            $kullanici = new Kullanici();
            $kullanici->adsoyad = request("adsoyad");
            $kullanici->kullanici_adi = request("kullanici_adi");
            $kullanici->email = request("email");
            $kullanici->sifre = Hash::make(request("sifre"));
            $kullanici->aktivasyon_anahtari = Str::Random(60);
            $kullanici->aktif_mi = 0;
            $kullanici->save();
            $kullanici->detay()->save(new KullaniciDetay());
        }

        Mail::to($kullanici->email)->send(new KullaniciKayitMail($kullanici));

        return redirect()->to("/")
            ->with("mesaj_tur", "success")
            ->with("pozisyon", "ustbar")
            ->with("mesaj", "Üyeliğiniz Başarıyla Kayıt Altına Alınmıştır. Şimdi mail adresinize gelen aktivasyon kodunu giriniz.");

    }

    public function aktiflestir($aktivasyon_anahtari)
    {
        $kullanici = Kullanici::where("aktivasyon_anahtari", $aktivasyon_anahtari)->first();
        if (!is_null($kullanici)) {
            $kullanici->aktif_mi = 1;
            $kullanici->aktivasyon_anahtari = null;
            $kullanici->save();
            auth()->login($kullanici);

            return redirect()->route("anasayfa")
                ->with("mesaj_tur", "success")
                ->with("pozisyon", "ustbar")
                ->with("mesaj", "Aktivasyon İşleminiz başarıyla tamamlanmıştır.");

        } else {
            return redirect()->route("anasayfa")
                ->with("mesaj_tur", "warning")
                ->with("pozisyon", "ustbar")
                ->with("mesaj", "Aktivasyon İşleminiz tamamlanamadı.");
        }
    }
}
