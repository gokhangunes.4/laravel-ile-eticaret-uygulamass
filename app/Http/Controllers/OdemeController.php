<?php

namespace App\Http\Controllers;

use Cart;
use App\Models\Sepet;
use App\Models\Siparis;
use Illuminate\Http\Request;


class OdemeController extends Controller
{
    public function index()
    {

        if (!auth()->check()) {
            return redirect()->route("kullanici.oturumac")
                ->with("mesaj_tur", "info")
                ->with("mesaj", "Odeme işlemi için oturum açmanız veya kayıt olmanız gerekmektedir.")
                ->with("pozisyon", "ustbar");
        } elseif ((Cart::count()) == 0) {
            return redirect()->route("sepet")
                ->with("mesaj_tur", "info")
                ->with("pozisyon", "ustbar")
                ->with("mesaj", "Odeme sayfasına gidebilmek için sepetinizde ürün bulunması gerekmektedir.");
        } else {
            $kullaniciDetay = auth()->user()->detay;
            return view("odeme", compact("kullaniciDetay"));
        }
    }

    public function odemeyap()
    {
        $siparis = request()->all();
        $siparis["sepet_id"] = session("aktif_sepet_id");
        $siparis["banka"] = "Enpara";
        $siparis["taksit_sayisi"] = 1;
        $siparis["durum"] = "Siparisiniz Alındı";
        $siparis["siparis_tutari"] = Cart::subtotal();

        Siparis::create($siparis);

        Cart::destroy();
        session()->forget('aktif_sepet_id');

        return redirect()->route('siparisler')
            ->with('mesaj_tur','succes')
            ->with('mesaj','basariyla kayit edildi');
    }
}
