<?php

namespace App\Mail;

use App\Models\Kullanici;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class KullaniciKayitMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Kullanici
     */
    public $kullanici;

    /**
     * KullaniciKayitMail constructor.
     *
     * @param $kullanici
     */
    public function __construct($kullanici)
    {
        $this->kullanici = $kullanici;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from("gokhangunes.4@gmail.com")
            ->subject(config("app.name")." - Kullanıcı Kayıt")
            ->view('mail.kullanicikayit');
    }
}
