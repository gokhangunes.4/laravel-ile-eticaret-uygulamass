<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Siparis extends Model
{
    use SoftDeletes;
    protected $table = "siparis";
    protected $fillable = ['sepet_id', 'siparis_tutari', 'telefon', 'banka', 'durum', 'adsoyad', 'adres', 'ceptelefonu', 'taksit_sayisi'];
    const CREATED_AT = "olusturma_tarihi";
    const UPDATED_AT = "guncelleme_tarihi";
    const DELETED_AT="silinme_tarihi";


    public function sepet(){
        return $this->belongsTo("App\Models\Sepet","sepet_id");
    }

    public function urunler(){
        return $this->sepet()->first()->sepeturun()->get();
    }

    /**
     * @return float
     */
    public function siparisTutari(): float
    {
        return (float) ((100+config('cart.tax'))/100)  * $this->siparis_tutari;
    }

    /**
     * @return float
     */
    public function kdvTutari(): float
    {
        return (float) (((100+config('cart.tax'))/100)  * $this->siparis_tutari) - $this->siparis_tutari;
    }
}
