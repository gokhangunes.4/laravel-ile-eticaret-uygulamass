<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sepet extends Model
{
    use SoftDeletes;

    protected $table = "sepet";
    protected $guarded = [];
    const UPDATED_AT = "guncelleme_tarihi";
    const CREATED_AT = "olusturma_tarihi";
    const DELETED_AT = "silinme_tarihi";

    public function sepeturun()
    {
        return $this->hasMany("App\Models\SepetUrun", "sepet_id");

        // hasOne Burda SepetUrun Tablosunda sepet_id kısmı ile bu modele ait sepet'in benzersiz idsini id kısmı eşitleyerek birleştirir.
    }

    public function kullanici()
    {
        return $this->belongsTo("App\Models\Kullanici");
        // belongsTo Burda Bu modele ait sepet tablosundaki kullanici_id ile kullanıcı modeline ait kullanici tablosunun benzersiz id sini id kısmını eşitler
    }

    public function urunler()
    {
        return $this->belongsToMany("App\Models\Urun", "sepet_urun", "sepet_id");
        // belongsToMant Burda bu modelde sepet modelindeki id kısmı ile sepet_urun deki sepet_id kısmını eşitleyerek bu verilere ait urun_id si ile uurn modeline ait urun tablosundaki id kısmı eşit kısımları birleştirir.
    }

    public function siparis()
    {
        return $this->hasOne("App\Models\Siparis");
    }

    /**
     * @return string
     */
    public static function aktifSepetId(): string
    {
        $aktifSepet = DB::table('sepet')
            ->leftJoin('siparis', 'siparis.sepet_id', '=', 'sepet.id')
            ->where('sepet.kullanici_id', auth()->id())
            ->whereRaw('siparis.sepet_id is null')
            ->orderBy('sepet.olusturma_tarihi')
            ->select('sepet.id')
            ->first();
        if (is_null($aktifSepet)) {

            $aktifSepet =  Sepet::create(
                ["kullanici_id" => auth()->id()]
            );

        }
        $aktirSepetId = $aktifSepet->id;

        return $aktirSepetId;
    }

    /**
     * @return int
     */
    public function sepetUrunAdet(): int
    {
        return DB::table('sepet_urun')->where('sepet_id', $this->id)->sum('adet');
    }
}
