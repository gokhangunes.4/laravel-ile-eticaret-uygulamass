<?php

namespace App\Models;

use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Kullanici extends Authenticatable
{
    use SoftDeletes;
    protected $table = "kullanici";
    protected $fillable = ['kullanici_adi','adsoyad', 'email', 'sifre','aktif_mi','aktivasyon_anahtari'];
    protected $hidden = ['sifre', 'aktivasyon_anahtari'];
    const UPDATED_AT = "guncelleme_tarihi";
    const CREATED_AT = "olusturma_tarihi";
    const DELETED_AT = "silinme_tarihi";
    public function getAuthPassword()
    {
        return $this->sifre;
    }
    public function sepetler(){
        return $this->hasMany("App\Models\Sepet","kullanici_id");
    }

    public function detay(){
        return $this->hasOne("App\Models\KullaniciDetay","kullanici_id")->withDefault();
    }

    public function sepetDoldur()
    {

        $aktif_sepet_id = Sepet::aktifSepetId();
        session()->put("aktif_sepet_id", $aktif_sepet_id);

        //Sepetteki ürünleri veritabanına ekle güncelle
        foreach (Cart::content() as $cartItem) {
            SepetUrun::updateOrCreate(
                ["sepet_id" => $aktif_sepet_id, "urun_id" => $cartItem->id],
                ["adet" => $cartItem->qty, "fiyati" => $cartItem->price, "durum" => "beklemede"]
            );
        }

        //Sepeti Boşalt
        Cart::destroy();
        foreach (auth()->user()->sepetler()->orderBy("id", "desc")->first()->sepeturun as $sepetUrun) {
            $urun = $sepetUrun->urun;
            Cart::add($sepetUrun->urun_id, $urun->urun_adi, $sepetUrun->adet, $urun->fiyati, ["slug" => $urun->slug]);
        }

        return true;
    }
}
